#!/bin/bash
trap err_out ERR

# functions
ERROR=""
function err_out {
    echo $ERROR
    exit 1
}

function get_kernel_version {
    eselect kernel list |\
    sed -rn 's,^.*linux-(\S+)-gentoo\s+\*,\1,p'
}

function get_boot {
    find /boot -maxdepth 1 -type f |\
    egrep '(config|initramfs|System.map|vmlinuz)' |\
    fgrep -v "$KERNEL_VERSION"
}

function get_modules {
    find /lib/modules -maxdepth 1 -type d -a -iname '*.*.*-gentoo' |\
    fgrep -v "$KERNEL_VERSION"
}

function get_sources {
    find /usr/src -maxdepth 1 -type d -a -iname 'linux-*.*.*-gentoo' |\
    fgrep -v "$KERNEL_VERSION"
}

# mount /boot
ERR="Could not mount /boot"
if ! ( mountpoint /boot > /dev/null 2>&1 ); then
    sudo mount /boot
fi

# Get selected kernel version
ERR="Could not kernel version"
KERNEL_VERSION=`get_kernel_version`

# Do an emerge dep-clean, to ensure
# we have unmerged old kernels
ERR="Could not portage dep-clean"
sudo emerge -c

# Get list of files in /boot to delete
ERR="Could not find old kernel files"
BOOT_DELETE=(`get_boot`)

# Get list of module directories to delete
ERR="Could not find old kernel modules"
MODULE_DELETE=(`get_modules`)

# Get list of linux sources to delete
ERR="Could not find old linux sources"
SOURCES_DELETE=(`get_sources`)

# Delete all old kernels, and kernel artifacts
ERR="Could not delete old kernel files"
sudo rm -rf ${BOOT_DELETE[@]} ${MODULE_DELETE[@]} ${SOURCES_DELETE[@]}

# re configure grub
ERR="grub configuration failure"
sudo grub-mkconfig -o /boot/grub/grub.cfg
