#!/bin/bash
set -ex

sudo tlmgr update --self
sudo tlmgr install latexmk
sudo tlmgr install enumitem
sudo tlmgr install noindentafter
sudo tlmgr install nicematrix
sudo tlmgr install xpatch
sudo tlmgr install units
sudo tlmgr install fontspec
sudo tlmgr install xypic
sudo tlmgr install rsfs
sudo tlmgr install romannum
sudo tlmgr install stdclsdv
