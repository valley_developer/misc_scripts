#!/bin/bash

SRC_PATH="/home/law/tools"
DEST_PATH="/usr/local/bin"

declare -A install_map
install_map["hibernate.sh"]="hbnt"
install_map["sleep.sh"]="slp"
install_map["upgrade_kernel.sh"]="upk"
install_map["clean_kernel.sh"]="clnk"
install_map["get_mouse_window.sh"]="gmw"
install_map["send_pass_to_window.sh"]="spw"
install_map["lock_wm.sh"]="lkwm"

for f in $( ls $SRC_PATH ); do
    f=$( basename "$f" );

    if [[ -n "${install_map[$f]}" ]]; then
        sudo install -o root -g law -m 0554 "${SRC_PATH}/$f" "${DEST_PATH}/${install_map[$f]}"
    fi
done
