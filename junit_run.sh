#!/bin/bash

env CLASSPATH="/usr/share/junit-4/lib/junit.jar:/usr/share/hamcrest-core-1.3/lib/hamcrest-core.jar:.:$CLASSPATH" java org.junit.runner.JUnitCore $@
