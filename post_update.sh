#!/bin/bash

set -ex

sudo grub-install
sudo grub-mkconfig -o /boot/grub/grub.cfg
sudo firecfg
