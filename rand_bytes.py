from pathlib import Path
from sys import argv


def get_rand_bytes(n):
    dev_random = Path('/dev') / 'random'
    with dev_random.open('rb') as fh:
        return fh.read(n)


def get_rand_hex(n_bytes):
    rand_bytes = get_rand_bytes(n_bytes)
    return '0x' + rand_bytes.hex()


def main():
    print(get_rand_hex(int(argv[1])))


if __name__ == '__main__':
    main()
