#!/bin/bash

function get_mouse_window() {
    xdotool getmouselocation -s 2> /dev/null |\
    sed -n '/WINDOW=/ s/WINDOW=//p'
}

# allow for username entry, this will
# be useful for web, and gui apps.
USER_NAME=

# default to ssh password, this is common
PASS_NAME=ssh

# default to mouse window location,
# will be convenient.
WINDOW_ID=`get_mouse_window`

while getopts ":p:u:w:" OPT; do
    case $OPT in
        p)
            PASS_NAME=$OPTARG;
            ;;

        u)
            USER_NAME=$OPTARG;
            ;;

        w)
            WINDOW_ID=$OPTARG;
            ;;

        ?)
            echo "Invalid option $OPTARG";
            ;;
    esac
done;

if [ -n "$USER_NAME" ]; then
    xdotool type --window $WINDOW_ID "$USER_NAME";
    xdotool key --window $WINDOW_ID 'Tab';
fi

# Type the password into the window
# pass show will send the return.
pass show $PASS_NAME |\
xargs -0 xdotool type --window $WINDOW_ID;
