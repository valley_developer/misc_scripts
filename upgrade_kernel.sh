#!/bin/bash

trap err_out ERR

LINUX_SOURCES=/usr/src/linux

ERR=""
function err_out {
    echo $ERR
    exit 1;
}

# mount /boot
sudo mount /boot

# find highest version configuration
HIGHEST_VERSION_CONFIG=""
VERSIONS=(`find /boot -maxdepth 1 -mindepth 1 -type f -a -iname 'config-*-gentoo' | sort -V`)
if [[ ${#VERSIONS[@]} = 0 ]]; then
    ERR="No gentoo linux kernel configurations available."
    err_out

else
    HIGHEST_VERSION_CONFIG=${VERSIONS[-1]}

fi

# configure new kernel
ERR="Could not configure new kernel."
sudo cp $HIGHEST_VERSION_CONFIG $LINUX_SOURCES/.config
sudo make -C $LINUX_SOURCES -s silentoldconfig

# rebuild external modules
ERR="Could not rebuild external modules."
sudo make -C $LINUX_SOURCES -s modules_prepare
sudo emerge @module-rebuild
sudo emerge @x11-module-rebuild

# build and install the kernel, and modules
ERR="Could not build, or could not install kernel."
sudo make -C $LINUX_SOURCES -s -j6
sudo make -C $LINUX_SOURCES -s modules_install
sudo make -C $LINUX_SOURCES -s install

# build, and install initramfs
ERR="Could not construct and install initramfs."
sudo genkernel --install initramfs

# configure grub
ERR="Could not configure grub."
sudo grub-mkconfig -o /boot/grub/grub.cfg
