#!/usr/bin/env bash

set -ex

pushd ~/proj/cpython
git clean -xdf
git co "v$1"
popd

rm -rf ~/tmp/python/build
mkdir -p ~/tmp/python/build
pushd ~/tmp/python/build
~/proj/cpython/configure \
    --prefix=/home/law/opt/"python-$1" \
    --enable-optimizations \
    --with-pymalloc
make -j8
make install
popd

pushd ~/opt/bin/
rm -f python3 pip3
ln -s "/home/law/opt/python-$1/bin/python3" .
ln -s "/home/law/opt/python-$1/bin/pip3" .
popd
